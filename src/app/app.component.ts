import { Component, OnInit } from '@angular/core';
import { ServerResponse } from './interfaces/server-response';
import { Server } from './interfaces/server.interface';
import { MinecraftService } from './services/minecraft.service';

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
    servers: Server[] = [];

    constructor(private minecraftService: MinecraftService) {}

    ngOnInit(): void {
        const domains = [
            'minecraft.gabrimagic.com',
            'forever-stranded.gabrimagic.com',
            'sirox.gabrimagic.com',
            'gabrimagic.com:25568'
        ];

        domains.forEach(domain => {
            this.minecraftService
                .getServerInfo(domain)
                .subscribe((data: ServerResponse): void => {
                    if (data.online) {
                        const server: Server = {
                            motd: data.motd.html.join(' '),
                            ip: data.ip,
                            port: data.port,
                            players: data.players.online,
                            maxPlayers: data.players.max,
                            version: data.version,
                            online: data.online,
                            image: data.icon,
                            hostname: domain,
                            mods: data.mods?.names
                        };
                        this.servers.push(server);
                    }
                });
        });
    }

    toggleMods(server: Server): void {
        server.showMods = !server.showMods;
    }
}

export interface Server {
    motd: string;
    ip: string;
    port: number;
    players: number;
    maxPlayers: number;
    version: string;
    online: boolean;
    image?: string;
    hostname: string;
    mods: string[];
    showMods?: boolean;
}

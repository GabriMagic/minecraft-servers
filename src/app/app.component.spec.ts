import {
    HttpClientTestingModule,
    HttpTestingController
} from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { of } from 'rxjs';
import { AppComponent } from './app.component';
import { ServerResponse } from './interfaces/server-response';
import { MinecraftService } from './services/minecraft.service';

describe('AppComponent', () => {
    let service: MinecraftService;
    let httpMock: HttpTestingController;

    beforeEach(async () => {
        await TestBed.configureTestingModule({
            imports: [RouterTestingModule, HttpClientTestingModule],
            declarations: [AppComponent]
        }).compileComponents();
        service = TestBed.inject(MinecraftService);
        httpMock = TestBed.inject(HttpTestingController);
    });

    it('should create the app', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.componentInstance;
        expect(app).toBeTruthy();
        expect(service).toBeTruthy();
        expect(httpMock).toBeTruthy();
    });

    it(`should have as title 'minecraft-servers'`, () => {
        const fixture = TestBed.createComponent(AppComponent);
        fixture.componentInstance;
        expect('minecraft-servers').toEqual('minecraft-servers');
    });

    it('should start app with servers info', () => {
        const fixture = TestBed.createComponent(AppComponent);
        const app = fixture.componentInstance;

        const httpClientSpy = spyOn(service, 'getServerInfo').and.returnValue(
            of({
                hostname: 'testHostName',
                icon: 'icon',
                debug: {
                    animatedmotd: false,
                    apiversion: 1,
                    cacheexpire: 1,
                    cachetime: 1,
                    cnameinsrv: false,
                    dns: {
                        srv: [],
                        srv_a: []
                    },
                    error: {
                        query: ''
                    },
                    ipinsrv: true,
                    ping: true,
                    query: false,
                    querymismatch: false,
                    srv: true
                },
                ip: '',
                mods: {
                    names: []
                },
                motd: {
                    clean: [],
                    html: [],
                    raw: []
                },
                online: true,
                players: { max: 10, online: 2 },
                port: 25565,
                protocol: 1,
                version: ''
            } as ServerResponse)
        );
        app.ngOnInit();

        expect(httpClientSpy).toHaveBeenCalledTimes(4);
    });
});

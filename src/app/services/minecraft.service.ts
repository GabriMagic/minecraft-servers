import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { ServerResponse } from '../interfaces/server-response';

@Injectable({
    providedIn: 'root'
})
export class MinecraftService {
    constructor(private http: HttpClient) {}

    getServerInfo(domain: string): Observable<ServerResponse> {
        const url = `https://api.mcsrvstat.us/2/${domain}`;

        return this.http.get<ServerResponse>(url);
    }
}

import { HttpClient } from '@angular/common/http';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TestBed } from '@angular/core/testing';
import { of } from 'rxjs';
import { ServerResponse } from '../interfaces/server-response';

import { MinecraftService } from './minecraft.service';

describe('MinecraftService', () => {
    let service: MinecraftService;

    beforeEach(() => {
        TestBed.configureTestingModule({
            imports: [HttpClientTestingModule]
        });
        service = TestBed.inject(MinecraftService);
    });

    it('should be created', () => {
        expect(service).toBeTruthy();
    });

    it('should get server-info response', () => {
        const expectedResult = {
            hostname: 'testHostName',
            icon: 'icon',
            debug: {
                animatedmotd: false,
                apiversion: 1,
                cacheexpire: 1,
                cachetime: 1,
                cnameinsrv: false,
                dns: {
                    srv: [],
                    srv_a: []
                },
                error: {
                    query: ''
                },
                ipinsrv: true,
                ping: true,
                query: false,
                querymismatch: false,
                srv: true
            },
            ip: '',
            mods: {
                names: []
            },
            motd: {
                clean: [],
                html: [],
                raw: []
            },
            online: true,
            players: { max: 10, online: 2 },
            port: 25565,
            protocol: 1,
            version: ''
        } as ServerResponse;

        const httpClientSpy = spyOn(
            TestBed.inject(HttpClient),
            'get'
        ).and.returnValue(of(expectedResult));

        service.getServerInfo('').subscribe(res => {
            expect(httpClientSpy).toHaveBeenCalledTimes(1);
            expect(res.hostname).toEqual(expectedResult.hostname);
        });
    });
});
